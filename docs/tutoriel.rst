.. Comment faire un gâteau au chocolat
=================
Comment faire un gâteau chocolat
=================

========================================================================================================
Indices and tables
==================
.. autosummary::
   :toctree: modules

* :ref:`RST Étape 1`
* :ref:`RST Étape 2`
* :ref:`RST Étape 3`
* :ref:`RST Étape 4`
* :ref:`RST Étape 5`
* :ref:`RST Étape 6`

.. _RST Étape 1:

1ère étape  
**********

Préchauffez votre four à 180°C (thermostat 6). Dans une casserole, faites fondre le chocolat et le beurre coupé en morceaux à feu très doux.

.. _RST Étape 2:

2ème étape
**********

Dans un saladier, ajoutez le sucre, les oeufs, la farine. Mélangez.

.. _RST Étape 3:

3ème étape
**********

Ajoutez le mélange chocolat/beurre. Mélangez bien.

.. _RST Étape 4:

4ème étape
**********

Beurrez et farinez votre moule puis y versez la pâte à gâteau.

.. _RST Étape 5:

5ème étape
**********

Faire cuire au four environ 20 minutes.

.. _RST Étape 6:

6ème étape
**********

A la sortie du four le gâteau ne paraît pas assez cuit. C'est normal, laissez-le refroidir puis démoulez-le.

.. sectionauthor:: Adrien Wargnier <test@gmail.com>
