.. Expressions française

TEST
========================================================================================================

Indices and tables
==================
.. autosummary::
   :toctree: modules

   foobar.foo
   foobar.bar
   foobar.bar.baz


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

