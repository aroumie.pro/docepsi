.. Expressions française
=================
Documentation sur les expressions française
=================

========================================================================================================
Indices and tables
==================
.. autosummary::
   :toctree: modules

* :ref:`RST Amour`
* :ref:`RST Anatomie`
    * :ref:`RST Un`
    * :ref:`RST Deux`
    * :ref:`RST Trois`
    * :ref:`RST Quatre`
* :ref:`En cours`

.. _RST Amour:

Amour
**********

* avoir dans la peau
.. note::
Dans son dictionnaire étymologique de la langue française[1], Walther Von Wartburg mentionne une expression approchante : « être saoul de la peau de quelqu’un ». Cette expression, datée de 1658, signifiait alors « avoir la passion pour quelqu’un ».
À la même époque, Molière utilisa l’expression « donner envie de sa peau » pour « rendre amoureux » dans la pièce Le dépit amoureux. Le personnage Marinette dit à un autre (Gros René) « Ardez le gros museau, pour nous donner envie de sa peau » (Acte 4, scène 4). Au XVIIIe siècle on disait familièrement, lorsqu’une femme voulait se marier avec un homme, qu’elle avait « envie de sa peau ». Pour preuve, on trouve cette définition dans le dictionnaire Furetière[2] de 1701 et dans le dictionnaire de l’académie française de 1798.
À la fin du XIXe siècle apparait enfin cette expression. Toutefois, la notion de passion n’y est pas encore présente. En effet la définition associée est « avoir de l’affection pour quelqu’un ». C’est notamment ce que l’on trouve dans le dictionnaire de l’argot de Delesalle de 1896.
L’expression se popularise à partir de la fin du XIXe siècle et reste, de nos jours, très couramment employée.

* avoir un coup de foudre
* avoir le coup de foudre
* conter fleurette
* coup de foudre
* courir le guilledou
* faire de l’œil
* faire des yeux de merlan frit
* faire les yeux doux
* regarder avec les yeux de l'amour
* taper dans l’œil

.. _RST Anatomie:

Anatomie
**********

.. _RST Un:

Ailes, pattes
-------------
* avoir du plomb dans l’aile
* avoir un fil à la patte
* avoir un sacré coup de patte
* donner des ailes
* faire patte de velours
* graisser la patte
* montrer patte blanche
* ne pas casser trois pattes à un canard
* prendre sous son aile
* voler de ses propres ailes

.. _RST Deux:

Bras
-------------
* avoir le bras long
* coûter un bras (et une jambe)
* en garder sous le coude
* être dans les bras de Morphée
* les bras m’en tombent
* lever le coude
* ne pas se moucher du coude
* rester les bras croisés
* se retrousser les manches
* se serrer les coudes
* tomber dans les bras de Morphée

.. _RST Trois:

Coeur
-------------
* tà cœur perdu
* tà cœur vaillant rien d’impossible
* tavoir bon cœur
* tavoir le cœur au bord des lèvres
* tavoir le cœur brisé / briser le cœur
* tavoir le cœur sec
* tavoir le cœur gros
* tavoir le cœur sur la main
* tavoir mal au cœur
* tavoir un cœur d’artichaut
* tavoir un cœur de pierre
* tavoir un pincement au cœur
* tcoup de cœur
* tdire ce qu’on a sur le cœur
* tfaire contre mauvaise fortune bon cœur
* tfaire la bouche en cœur
* tfaire le joli cœur
* tmains froides, cœur chaud
* touvrir son cœur
* tprendre à cœur

.. _RST Quatre:

Doigts
-------------

.. _RST En cours:

En cours
**********

.. sectionauthor:: Adham Roumie <test@gmail.com>
